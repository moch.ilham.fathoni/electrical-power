<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    protected $urladafruitio = 'https://io.adafruit.com/api/v2/ismansuga/feeds/';
    protected $iokey = 'aio_byIJ69Ka5D8w1m8XlAdUc8F0DdEE';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getData() {
        $w = 0;
        $w2 = 0;
        $w3 = 0;
        $jml = 0;
        $jml2 = 0;
        $jml3 = 0;
        $url = $this->urladafruitio . 'listrik-1/data/last';
        $url2 = $this->urladafruitio . 'listrik-2/data/last';
        $url3 = $this->urladafruitio . 'listrik-3/data/last';
        $separator = "#";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response_curl = curl_exec($curl);
        curl_close($curl);
        $response_curl = json_decode($response_curl, true);

        if (isset($response_curl['value'])) {
            $val = $response_curl['value'];
            if(strpos($val, $separator) !== false) {
                $arr = explode($separator, $val);
                if (is_numeric($arr[0]) && is_numeric($arr[1])) {
                    $w = $arr[0];
                    $jml = $arr[1];
                }
            }
        }

        $curl2 = curl_init();
        curl_setopt($curl2, CURLOPT_URL, $url2);
        curl_setopt($curl2, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl2, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
        $response_curl2 = curl_exec($curl2);
        curl_close($curl2);
        $response_curl2 = json_decode($response_curl2, true);

        if (isset($response_curl2['value'])) {
            $val = $response_curl2['value'];
            if(strpos($val, $separator) !== false) {
                $arr = explode($separator, $val);
                if (is_numeric($arr[0]) && is_numeric($arr[1])) {
                    $w2 = $arr[0];
                    $jml2 = $arr[1];
                }
            }
        }

        $curl3 = curl_init();
        curl_setopt($curl3, CURLOPT_URL, $url3);
        curl_setopt($curl3, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl3, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($curl3, CURLOPT_RETURNTRANSFER, true);
        $response_curl3 = curl_exec($curl3);
        curl_close($curl3);
        $response_curl3 = json_decode($response_curl3, true);

        if (isset($response_curl3['value'])) {
            $val = $response_curl3['value'];
            if(strpos($val, $separator) !== false) {
                $arr = explode($separator, $val);
                if (is_numeric($arr[0]) && is_numeric($arr[1])) {
                    $w3 = $arr[0];
                    $jml3 = $arr[1];
                }
            }
        }

        $data = array (
            "sensor1" => array("watt" => number_format($w,3), "total" => number_format($jml,3)),
            "sensor2" => array("watt" => number_format($w2,3), "total" => number_format($jml2,3)),
            "sensor3" => array("watt" => number_format($w3,3), "total" => number_format($jml3,3))
        );

        return $data;
    }

    public function getKwh() {
        $bulan_kemarin = date('Y-m-t', strtotime(date('Y-m-d')." -1 month"));
        $separator = "#";

        $kwh_bulan_kemarin = 0;
        $kwh_bulan_kemarin2 = 0;
        $kwh_bulan_kemarin3 = 0;
        $kwh_terakhir = 0;
        $kwh_terakhir2 = 0;
        $kwh_terakhir3 = 0;

        $url = $this->urladafruitio . 'listrik-1/data?limit=1&end_time='.$bulan_kemarin;
        $url2 = $this->urladafruitio . 'listrik-2/data?limit=1&end_time='.$bulan_kemarin;
        $url3 = $this->urladafruitio . 'listrik-3/data?limit=1&end_time='.$bulan_kemarin;
        $url_last = $this->urladafruitio . 'listrik-1/data/last';
        $url_last2 = $this->urladafruitio . 'listrik-2/data/last';
        $url_last3 = $this->urladafruitio . 'listrik-3/data/last';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url_last);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response_curl = curl_exec($curl);
        curl_close($curl);
        $response_curl = json_decode($response_curl, true);

        if (isset($response_curl['value'])) {
            $val = $response_curl['value'];
            if(strpos($val, $separator) !== false) {
                $arr = explode($separator, $val);
                if (is_numeric($arr[0]) && is_numeric($arr[1])) {
                    $kwh_terakhir = $arr[1];
                }
            }
        }

        $curl2 = curl_init();
        curl_setopt($curl2, CURLOPT_URL, $url_last2);
        curl_setopt($curl2, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl2, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
        $response_curl2 = curl_exec($curl2);
        curl_close($curl2);
        $response_curl2 = json_decode($response_curl2, true);

        if (isset($response_curl2['value'])) {
            $val2 = $response_curl2['value'];
            if(strpos($val2, $separator) !== false) {
                $arr = explode($separator, $val2);
                if (is_numeric($arr[0]) && is_numeric($arr[1])) {
                    $kwh_terakhir2 = $arr[1];
                }
            }
        }

        $curl3 = curl_init();
        curl_setopt($curl3, CURLOPT_URL, $url_last3);
        curl_setopt($curl3, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl3, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($curl3, CURLOPT_RETURNTRANSFER, true);
        $response_curl3 = curl_exec($curl3);
        curl_close($curl3);
        $response_curl3 = json_decode($response_curl3, true);

        if (isset($response_curl3['value'])) {
            $val3 = $response_curl3['value'];
            if(strpos($val3, $separator) !== false) {
                $arr = explode($separator, $val3);
                if (is_numeric($arr[0]) && is_numeric($arr[1])) {
                    $kwh_terakhir3 = $arr[1];
                }
            }
        }

        $curlkemarin = curl_init();
        curl_setopt($curlkemarin, CURLOPT_URL, $url);
        curl_setopt($curlkemarin, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curlkemarin, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($curlkemarin, CURLOPT_RETURNTRANSFER, true);
        $response_curlold = curl_exec($curlkemarin);
        curl_close($curlkemarin);
        $response_curlold = json_decode($response_curlold, true);

        if ($response_curlold) {
            $val = $response_curlold[0]['value'];
            if(strpos($val, $separator) !== false) {
                $arr = explode($separator, $val);
                if (is_numeric($arr[0]) && is_numeric($arr[1])) {
                    $kwh_bulan_kemarin = $arr[1];
                }
            }
        }

        $curlkemarin2 = curl_init();
        curl_setopt($curlkemarin2, CURLOPT_URL, $url2);
        curl_setopt($curlkemarin2, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curlkemarin2, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($curlkemarin2, CURLOPT_RETURNTRANSFER, true);
        $response_curlold2 = curl_exec($curlkemarin2);
        curl_close($curlkemarin2);
        $response_curlold2 = json_decode($response_curlold2, true);

        if ($response_curlold2) {
            $val = $response_curlold2[0]['value'];
            if(strpos($val, $separator) !== false) {
                $arr = explode($separator, $val);
                if (is_numeric($arr[0]) && is_numeric($arr[1])) {
                    $kwh_bulan_kemarin2 = $arr[1];
                }
            }
        }

        $curlkemarin3 = curl_init();
        curl_setopt($curlkemarin3, CURLOPT_URL, $url3);
        curl_setopt($curlkemarin3, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curlkemarin3, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($curlkemarin3, CURLOPT_RETURNTRANSFER, true);
        $response_curlold3 = curl_exec($curlkemarin3);
        curl_close($curlkemarin3);
        $response_curlold3 = json_decode($response_curlold3, true);

        if ($response_curlold3) {
            $val = $response_curlold3[0]['value'];
            if(strpos($val, $separator) !== false) {
                $arr = explode($separator, $val);
                if (is_numeric($arr[0]) && is_numeric($arr[1])) {
                    $kwh_bulan_kemarin3 = $arr[1];
                }
            }
        }

        $data = array (
            "sensor1" => array("kwh" => number_format($kwh_terakhir - $kwh_bulan_kemarin,3)),
            "sensor2" => array("kwh" => number_format($kwh_terakhir2 - $kwh_bulan_kemarin2,3)),
            "sensor3" => array("kwh" => number_format($kwh_terakhir3 - $kwh_bulan_kemarin3,3))
        );

        return $data;
    }

}
