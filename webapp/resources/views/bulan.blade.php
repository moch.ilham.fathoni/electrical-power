@extends('layouts.app')

@section('onBulan')
active
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Data KWH Bulan {{ date('F') }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                   
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
        <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Pelanggan 1</h5>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row invoice-info">
                                <div class="col-12">
                                    <strong>Total Pemakaian Kwh</strong><br>
                                    <div id="kwh-1">0 KWH</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Pelanggan 2</h5>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row invoice-info">
                                <div class="col-12">
                                    <strong>Total Pemakaian Kwh</strong><br>
                                    <div id="kwh-2">0 KWH</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Pelanggan 3</h5>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row invoice-info">
                                <div class="col-12">
                                    <strong>Total Pemakaian Kwh</strong><br>
                                    <div id="kwh-3">0 KWH</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
@endsection

@push('scripts')
<script>
    $(function() {
        var kwh = document.getElementById("kwh-1");
        var kwh2 = document.getElementById("kwh-2");
        var kwh3 = document.getElementById("kwh-3");

        function reloadData() {
            $.ajax({
                url: "{{ route('kwh') }}",
                type: "GET",
                data: {
                    format: 'json',
                    _token:"{{ csrf_token() }}"
                },
                sasdataType: 'json',
                success: function(response){
                    $.each(response, function(index, item){
                        if (index == "sensor1") {
                            kwh.innerHTML = item['kwh'] + " KWH";
                        }
                        if (index == "sensor2") {
                            kwh2.innerHTML = item['kwh'] + " KWH";
                        }
                        if (index == "sensor3") {
                            kwh3.innerHTML = item['kwh'] + " KWH";
                        }
                    });
                    console.log(response);
                }
            });
        }

        setInterval(function () {
            reloadData();
        }, 10000); // update data di web setiap 10 detik
    });
</script>
@endpush